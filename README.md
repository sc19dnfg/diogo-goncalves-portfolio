# Diogo-Goncalves-Portfolio

## Description
Hello to anyone reading this, and welcome to my personal portfolio!

This portfolio contains details about several different projects that I've been involved in throught all my studies within the field of Computer Science. Some of them are more recent than others, but all of them showcase a different side of myself and my capabilities as a professional.

I believe that, above all, this portfolio successfully showcases my evolution throughout the years in my work style, my chosen fields of interest and professional capabilities. More recent projects tend to be more complex, use more updated technologies and overall contain better structured code.

Some of the projects here have been developed as a group, others solo. Some are purely research oriented, others were developed with the intent to be used in the real world. All of them are equally important in their own way.

Aside from the Masters project, all others listed here are finished already. Each branch of this repository contains a different project. These projects are but a snippet of everything I've done throughout my studies, having been chosen for resulting, in my opinion, in the most interesting and complete deliverables.

Feel free to browse this repository, and don't hesitate to ask any questions by contacting me via email: diogonuno.uk@gmail.com


Diogo Nuno Fernandes Goncalves